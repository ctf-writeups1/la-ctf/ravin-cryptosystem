# ravin-cryptosystem    425


## Description 

I don't really understand why people choose big numbers that can't be factored. No one has been able to crack my RSA implementation even though my modulus is factorable. It should just be normal RSA??? All I did was make it faster. I've asked my friends and the only feedback I've gotten so far are rave reviews about how secure my algorithm is. Hmm, maybe I'll name it the Ravin Cryptosystem. There better not be anyone with a similar idea.

files : output.txt, ravin.py

## Analyse

Ok so let's try a difficult one. First, we can see an output.txt, with result values like RSA encryption n, e and c.

n is the prod of 2 generated primes number p and q, and it's the public key

c is the cipher text.

e is 65537 and is a number that define the number of cipher iterration.

It's seems like we have to decrypt the cipher in the output.txt.

Testing classic RSA decrypt doesn't work. Therefore, we easily factorize the n public key number into classic RSA p and q primes numbers : 

n = 996905207436360486995498787817606430974884117659908727125853

n = 861346721469213227608792923571 * 1157379696919172022755244871343

p = 861346721469213227608792923571

q = 1157379696919172022755244871343

Reading the python file, we realise that the cipher method could be very difficult to inverse, because it use remainds of euclidiean division multiple times.
There is a comment on the function fastpow. It's a clue.

Indeed, the author of CTF want us to think it's RSA cypher. For that, he put data like RSA in the script, but it's not what we think.

Decompose the Fastpow function :
```python
def fastpow(b, p, mod):
    # idk this is like repeated squaring or something i heard it makes pow faster
    a = 1
    while p:
        p >>= 1
        b = (b*b)%mod
        if p&1:
            a = (a*b)%mod
    return a
```
And it call like :
```python
fastpow(m, e, n) # m is the message to encode
```
So p is e, b is m and mod is n... What a mess...
Let's simplify :
```python
def fastpow(m, e=65537, n):
    # idk this is like repeated squaring or something i heard it makes pow faster
    a = 1
    while e:
        e >>= 1
        m = (m*m)%n
        if e&1:
            a = (a*m)%n
    return a
```
Ok, it's more clear. Now, we can decompose. on each iteration, the program remove the last bit of e, and do it while e is positive. 

As e is always 65537, we can make a try to describe the process of the function :
```python
e=65537
while e:
    e >>= 1
    print(bin(e))
    if e&1:
        print("Condition True")
```

    0b1000000000000000
    0b100000000000000
    0b10000000000000
    0b1000000000000
    0b100000000000
    0b10000000000
    0b1000000000
    0b100000000
    0b10000000
    0b1000000
    0b100000
    0b10000
    0b1000
    0b100
    0b10
    0b1
    Condition True
    0b0
We can determine differentes things with this test.
First, in the Fastpow function, the value a is initiated to 1, but 1 value is used in a product (so unless), and a is re-affected only one time before returning. We can simplify this in :
```python
def fastpow(m, e=65537, n):
    # idk this is like repeated squaring or something i heard it makes pow faster
    while e:
        e >>= 1
        m = (m*m)%n
        if e&1:
            return m%n
```
In facts, this function is exactly the same as :
```python
def simplify_fastpow(m, n):
    for i in range(16):
        m = (m*m)%n
    return m%n
```
Now, it's more clear. We have to compute the squared modular root of our cipher_text 16 times to retrieve our number...

But there is a trick. Indeed, like that, it's not possible to retrieve the good squared modular root of a non prime modulo number, because they can be multiple possibilities.
The name also give us a clue to resolve this : The Rabin Cryptosystem.

## Method

The rabin cryptosystem, like RSA, use 2 big prime numbers generated randomly p and q for generate the public key p*q = n. The private key IS the 2 numbers p and q. (Note that we already have private key p and q)

To decrypt with Rabin Cryptosystem, we have to accomplish multiple tasks. Let's make it with simple exemple, for one iteration.

Taking p = 7 and q = 11, the public key n = p * q = 77
Taking m = 20 (the message to encode), we compute cypher = (20*20) % 77 = 15

We have encrypt our message m = 20 to it's cypher representation c=15.

To decrypt, we know private key is 7 and 11.
First, we compute the square modular root of our cypher modulo p...

Yes, this time, we can, because our modulo p is a prime number. In this case, the Tonelli-shanks algorythm say :

For primes such that p ≡ 3 mod 4, this problem has possible solutions mp = c ** (p+1)//4 mod p if these satisfy r**2 ≡ n mod p, they are the only solutions.

So we compute :

        mp = c ** (p+1)//4 mod p
    <=> mp = 15 ** (7+1)//4 mod 7
    <=> mp = 225 mod 7
    <=> mp = 1
Same thing for mq, the equivalent of mp for other part of the key mq

        mq = c ** (q+1)//4 mod q
    <=> mq = 15 ** (11+1)//4 mod 11
    <=> mq = 3375 mod 11
    <=> mq = 9
Now, we again use the chineze remainder theorem to retrieve the 4 square roots possible for the system. Indeed, we first gonna find the Bezout coefficient u and v of mp and mq.

because mp\*u + mq\*v = 1, we can retrieve the 4 square roots possible.

I also have to pass demonstration, but using extended euclidean algorithm, we found u = -3 and v = 2. We can compute the 4 square as :

    r = ( u * p * mq + v * q * mp ) mod n
    -r = n - r
    s = ( u * p * mq - v * q * mp ) mod n
    -s = n - s
    <=>
    r = ( -3 * 7 * 9 + 2 * 11 * 1 ) mod 77 = 64
    -r = 77 - r = 77 - 64 = 13
    s = ( -3 * 7 * 9 - 2 * 11 * 1 ) mod 77 = 20
    -s = 77 - s = 77 - 20 = 57
    
Tada, we found our message 20. But we also have 4 other possibilities...
We have to try each combinaisons of 4 root for each 16 tentatives.

## Implementation

First, we code our function decrypt that gonna use our precedent function extended euclidean algorythm for Bezout coefficients, to decrypt one iteration of the rabin cryptosystem :
```python
def extended_euclidean_algorithm(a, b):
    """Solve function help me to compute the Euclidian
    algorithme of two numbers, for obtained PGCD of 
    the numbers, but also one Bezout coefficient, a pair 
    of u and v ints like au + bv = PGCD
    """
    if b == 0:
        return (a, 1, 0)
    gcd, u, v = extended_euclidean_algorithm(b, a % b)
    return (gcd, v, u - (a // b) * v)

def decrypt(ciphertext, p, q):
    n = p * q
    mp = pow(ciphertext, (p+1)//4, p)
    mq = pow(ciphertext, (q+1)//4, q)
    pgcd,u,v =extended_euclidean_algorithm(p,q)
    r=(u*p*mq + v*q*mp) % n
    nr=n-r
    s=(u*p*mq - v*q*mp) % n
    ns=n-s
    return r,nr,s,ns
```
We create a primary list with our cypher text c item
Then, for 16 times, we gonna iterate our list, and decrypt these value. We store the four possibles resulted values in the next list. At the end of iterations, we decrypt the bytes in string :
```python
cypherlist = [375444934674551374382922129125976726571564022585495344128269]
for i in range(16):
    nextlist = []
    for c in cypherlist:
        new_list+=decrypt(c, p, q)
    cypherlist = new_list
for i in cypherlist:
    print(i.to_bytes((i.bit_length() + 7) // 8, 'big'))
```
Trying like that doesn't work. Because the cypherlist is exponentially greater each iteration, the script can't accomplish it for 16 itterations.
Let's try with set :
```python
cypherset = {375444934674551374382922129125976726571564022585495344128269}
for i in range(16):
    nextset = set()
    for c in cypherset:
        nextset = nextset.union(decrypt(c, p, q))
    cypherset = nextset
for i in cypherset:
    print(i.to_bytes((i.bit_length() + 7) // 8, 'big'))
```
This time, we only have 4 results each time and it's really fast. We have the flag. 


