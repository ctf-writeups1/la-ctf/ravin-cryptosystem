def extended_euclidean_algorithm(a, b):
    """Solve function help me to compute the Euclidian
    algorithme of two numbers, for obtained PGCD of 
    the numbers, but also one Bezout coefficient, a pair 
    of u and v ints like au + bv = PGCD
    """
    if b == 0:
        return (a, 1, 0)
    gcd, u, v = extended_euclidean_algorithm(b, a % b)
    return (gcd, v, u - (a // b) * v)

def decrypt(ciphertext, p, q):
    n = p * q
    mp = pow(ciphertext, (p+1)//4, p)
    mq = pow(ciphertext, (q+1)//4, q)
    pgcd,u,v =extended_euclidean_algorithm(p,q)
    r=(u*p*mq + v*q*mp) % n
    nr=n-r
    s=(u*p*mq - v*q*mp) % n
    ns=n-s
    return r,nr,s,ns


p = 1157379696919172022755244871343
q = 861346721469213227608792923571
cypherset = {(375444934674551374382922129125976726571564022585495344128269)}
for i in range(16):
    nextset = set()
    for c in cypherset:
        nextset = nextset.union(set(list(decrypt(c, p, q))))
    cypherset = nextset
for i in cypherset:
    print(i.to_bytes((i.bit_length() + 7) // 8, 'big'))